#!/bin/bash

if [[ $# < 1 ]]; then
    echo -e "usage: megakey [url]"
    exit
fi

function urlb64_to_b64 {
	local b64=$(echo -n "$1" | tr '\-_' '+/' | tr -d ',')
	local pad=$(((4-${#1}%4)%4))
	for i in $(seq 1 $pad); do
		b64="${b64}="
	done
	echo -n "$b64"
}

#file_id=$(echo -n $1 | cut -d'!' -f2)
file_key=$(echo -n $1 | cut -d'!' -f3)

if [ -x "$(command -v xxd)" ]; then
	hex_raw_key=$(echo -n $(urlb64_to_b64 "$file_key") | base64 -d -i 2> /dev/null | xxd -p | tr -d '\n')
else
	hex_raw_key=$(echo -n $(urlb64_to_b64 "$file_key") | base64 -d -i 2> /dev/null | od -v -An -t x1 | tr -d '\n ')
fi

hex_key[0]=$((0x${hex_raw_key:0:16} ^ 0x${hex_raw_key:32:16}))
hex_key[1]=$((0x${hex_raw_key:16:16} ^ 0x${hex_raw_key:48:16}))
hex_key=$(printf "%016x" ${hex_key[*]})
hex_iv="${hex_raw_key:32:16}0000000000000000"
res="openssl enc -d -aes-128-ctr -K $hex_key -iv $hex_iv"

if [ -x "$(command -v termux-clipboard-set)" ]; then
	termux-clipboard-set $res
fi

echo $res